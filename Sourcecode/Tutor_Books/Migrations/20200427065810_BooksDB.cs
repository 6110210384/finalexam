﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Tutor_Books.Migrations
{
    public partial class BooksDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BooksCategory",
                columns: table => new
                {
                    BooksCategoryID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Subject = table.Column<string>(nullable: true),
                    place = table.Column<string>(nullable: true),
                    hour = table.Column<int>(nullable: false),
                    price = table.Column<string>(nullable: true),
                    time = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BooksCategory", x => x.BooksCategoryID);
                });

            migrationBuilder.CreateTable(
                name: "BooksList",
                columns: table => new
                {
                    BooksID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    BooksCategoryID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BooksList", x => x.BooksID);
                    table.ForeignKey(
                        name: "FK_BooksList_BooksCategory_BooksCategoryID",
                        column: x => x.BooksCategoryID,
                        principalTable: "BooksCategory",
                        principalColumn: "BooksCategoryID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BooksList_BooksCategoryID",
                table: "BooksList",
                column: "BooksCategoryID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BooksList");

            migrationBuilder.DropTable(
                name: "BooksCategory");
        }
    }
}
