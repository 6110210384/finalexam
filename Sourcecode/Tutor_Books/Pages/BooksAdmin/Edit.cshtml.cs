using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Tutor_Books.Data;
using Tutor_Books.Models;

namespace Tutor_Books.Pages_BooksAdmin
{
    public class EditModel : PageModel
    {
        private readonly Tutor_Books.Data.Tutor_BooksContext _context;

        public EditModel(Tutor_Books.Data.Tutor_BooksContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Books Books { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Books = await _context.BooksList
                .Include(b => b.BooksCat).FirstOrDefaultAsync(m => m.BooksID == id);

            if (Books == null)
            {
                return NotFound();
            }
           ViewData["BooksCategoryID"] = new SelectList(_context.BooksCategory, "BooksCategoryID", "Subject");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Books).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BooksExists(Books.BooksID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool BooksExists(int id)
        {
            return _context.BooksList.Any(e => e.BooksID == id);
        }
    }
}
