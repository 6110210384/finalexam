using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Tutor_Books.Data;
using Tutor_Books.Models;

namespace Tutor_Books.Pages_BooksAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly Tutor_Books.Data.Tutor_BooksContext _context;

        public DetailsModel(Tutor_Books.Data.Tutor_BooksContext context)
        {
            _context = context;
        }

        public Books Books { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Books = await _context.BooksList
                .Include(b => b.BooksCat).FirstOrDefaultAsync(m => m.BooksID == id);

            if (Books == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
