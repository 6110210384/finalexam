using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Tutor_Books.Data;
using Tutor_Books.Models;

namespace Tutor_Books.Pages_BooksAdmin
{
    public class CreateModel : PageModel
    {
        private readonly Tutor_Books.Data.Tutor_BooksContext _context;

        public CreateModel(Tutor_Books.Data.Tutor_BooksContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["BooksCategoryID"] = new SelectList(_context.BooksCategory, "BooksCategoryID", "Subject");
            return Page();
        }

        [BindProperty]
        public Books Books { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.BooksList.Add(Books);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}