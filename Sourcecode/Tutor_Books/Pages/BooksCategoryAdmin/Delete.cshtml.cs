using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Tutor_Books.Data;
using Tutor_Books.Models;

namespace Tutor_Books.Pages.BooksCategoryAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly Tutor_Books.Data.Tutor_BooksContext _context;

        public DeleteModel(Tutor_Books.Data.Tutor_BooksContext context)
        {
            _context = context;
        }

        [BindProperty]
        public BooksCategory BooksCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            BooksCategory = await _context.BooksCategory.FirstOrDefaultAsync(m => m.BooksCategoryID == id);

            if (BooksCategory == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            BooksCategory = await _context.BooksCategory.FindAsync(id);

            if (BooksCategory != null)
            {
                _context.BooksCategory.Remove(BooksCategory);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
