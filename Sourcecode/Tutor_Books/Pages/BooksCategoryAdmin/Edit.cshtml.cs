using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Tutor_Books.Data;
using Tutor_Books.Models;

namespace Tutor_Books.Pages.BooksCategoryAdmin
{
    public class EditModel : PageModel
    {
        private readonly Tutor_Books.Data.Tutor_BooksContext _context;

        public EditModel(Tutor_Books.Data.Tutor_BooksContext context)
        {
            _context = context;
        }

        [BindProperty]
        public BooksCategory BooksCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            BooksCategory = await _context.BooksCategory.FirstOrDefaultAsync(m => m.BooksCategoryID == id);

            if (BooksCategory == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(BooksCategory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BooksCategoryExists(BooksCategory.BooksCategoryID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool BooksCategoryExists(int id)
        {
            return _context.BooksCategory.Any(e => e.BooksCategoryID == id);
        }
    }
}
