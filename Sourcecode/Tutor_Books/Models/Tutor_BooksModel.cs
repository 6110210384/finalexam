using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Tutor_Books.Models
{
    public class NewsUser : IdentityUser{ 
        public string FirstName { get; set;} 
        public string LastName { get; set;} 
    }
    public class BooksCategory{
        public int BooksCategoryID { get; set;}
        public string Subject { get; set;}
        public string place { get; set;}
        public int hour { get; set;}
        public string price { get; set;}
        public string time { get; set;}
    }
     public class Books{
        public int BooksID { get; set;}
        public int BooksCategoryID { get; set;}
        public BooksCategory BooksCat { get; set;}

        public string NewsUserId {get; set;} 
        public NewsUser postUser {get; set;}
    }
}